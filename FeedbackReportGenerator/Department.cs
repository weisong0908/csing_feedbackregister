﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackReportGenerator
{
    public class Department
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public IList<string> Categories { get; set; } = new List<string>();
    }
}
