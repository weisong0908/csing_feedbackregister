﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeedbackReportGenerator
{
    public class Feedback
    {
        public int Id { get; set; }
        public DateTime DateOfFeedback { get; set; }
        public string CourseOrDepartment { get; set; }
        public string FeedbackNature { get; set; }
        public string DepartmentResponsible { get; set; }
        public string FeedbackCode { get; set; }
        public string IsRemoved { get; set; }
        public string IsExcludedFromAnalysis { get; set; }

        public int Year
        {
            get => DateOfFeedback.Year;
        }

        public int Quarter
        {
            get => (DateOfFeedback.Month + 2) / 3;
        }
    }
}
