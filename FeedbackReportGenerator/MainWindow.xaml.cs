﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Forms = System.Windows.Forms;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;

namespace FeedbackReportGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string exportPath = @"C:\Users\weisong.teng\Desktop\Feedback charts\";

        static int chartNumber;

        static DateTime reportEndDate = DateTime.Parse("2018-3-31");
        static int endYear = reportEndDate.Year;
        static int endQuarter = (reportEndDate.Month + 2) / 3;

        static IList<Feedback> feedbacks;
        static IList<Department> departments;
        static IList<string> categories;

        static IList<int> years = new List<int>()
        {
            2012,2013,2014,2015,2016,2017,2018
        };

        static IList<int> quarters = new List<int>()
        {
            1,2,3,4
        };

        static IList<string> feedbackNatures = new List<string>()
        {
            "Complaint", "Compliment", "Information"
        };

        static Excel.Application excel;
        static Excel.Workbook workbook;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_GenerateReport_Click(object sender, RoutedEventArgs e)
        {
            feedbacks = new List<Feedback>();
            ReadDatabase();

            departments = new List<Department>();
            SetDepartments();
            categories = GetAllCategories();

            excel = new Excel.Application();
            workbook = excel.Workbooks.Add();
            chartNumber = 1;

            foreach (var department in departments)
                CreateChart_FeedbackCategoryVsQuarter_Department_CurrentYear_Bar(department);

            //// MACRO
            //CreateChart_FeedbackNatureVsQuarter_AllTime_Bar();
            //CreateChart_FeedbackNatureVsQuarter_AllTime_Bar_PercentageOnly();
            //CreateChart_FeedbackNatureVsQuarter_CurrentYear_Bar();
            //CreateChart_FeedbackNature_CurrentQuarter_Bar();

            //// MIDDLE
            //CreateChart_FeedbackNatureVsDepartment_AllTime_Bar();
            //foreach (var department in departments)
            //    CreateChart_FeedbackNatureVsQuarter_AllTime_Bar(department);
            //foreach (var department in departments)
            //    CreateChart_FeedbackNatureVsQuarter_CurrentYear_Bar(department);

            //// MICRO
            //foreach (var department in departments)
            //    CreateChart_FeedbackCategoryAndFeedbackNature_Department_AllTime_Bar(department);
            //foreach (var feedbackNature in feedbackNatures)
            //    foreach (var category in categories)
            //        CreateChart_FeedbackCategory_FeedbackNature_AllTime_Bar(feedbackNature, category);
            //foreach (var department in departments)
            //    CreateChart_FeedbackCategory_Department_CurrentQuarter_Bar(department);

            //foreach(var department in departments)
            //{
            //    CreateChart_FeedbackCategoryAndFeedbacknature_department_allTime_bar(department);
            //}

            //CreateChart_FeedbackNature_currentQuarter_bar_percentageOnly();
            //CreateChart_FeedbackNatureVsDepartment_currentQuarter_bar();
            //foreach (var feedbackNature in feedbackNatures)
            //    CreateChart_FeedbackCategory_FeedbackNature_bar(feedbackNature);
            //foreach (var feedbackNature in feedbackNatures)
            //    CreateChart_FeedbackCategory_FeedbackNature_percentageOnly_bar(feedbackNature);

            //CreateChart_FeedbackNature_currentQuarter_pie();
            //CreateChart_FeedbackNatureVsQuarter_allTime_line();
            //CreateChart_FeedbackNatureVsDepartment_currentQuarter_bar_percentageOnly();
            //CreateChart_FeedbackNatureVsDepartment_allTime_line();
            //CreateChart_FeedbackNatureVsDepartment_allTime_bar_percentageOnly();

            //foreach (var department in departments)
            //    CreateChart_FeedbackCategory_currentQuarter_department_bar(department);

            //foreach (var department in departments)
            //    CreateChart_FeedbackCategory_currentQuarter_department_bar_percentageOnly(department);

            //foreach (var departmernt in departments)
            //    CreateChart_FeedbackCategoryVsQuarter_department_allTime_bar(departmernt);

            //foreach (var departmernt in departments)
            //    CreateChart_FeedbackCategoryVsQuarter_department_allTime_line(departmernt);

            //foreach (var departmernt in departments)
            //    CreateChart_FeedbackCategoryVsQuarter_department_allTime_bar_percentageOnly(departmernt);

            workbook.Close(SaveChanges: false);
            excel.Quit();

            MessageBox.Show("Done");
        }

        private static void SetDepartments()
        {
            departments.Add(new Department() { Name = "Academic", ShortName = "ACAD" });
            departments.Add(new Department() { Name = "Admissions", ShortName = "ADM" });
            departments.Add(new Department() { Name = "Facilities", ShortName = "FAC" });
            departments.Add(new Department() { Name = "Finance", ShortName = "FIN" });
            departments.Add(new Department() { Name = "Human Resource and Administration", ShortName = "HR" });
            departments.Add(new Department() { Name = "IT Support", ShortName = "IT" });
            departments.Add(new Department() { Name = "Library", ShortName = "LIB" });
            departments.Add(new Department() { Name = "Marketing", ShortName = "MKT" });
            departments.Add(new Department() { Name = "Navitas English", ShortName = "NEP" });
            departments.Add(new Department() { Name = "Quality and Compliance", ShortName = "QA" });
            departments.Add(new Department() { Name = "Student Administration", ShortName = "SA" });
            departments.Add(new Department() { Name = "Student Central", ShortName = "SC" });
            departments.Add(new Department() { Name = "Student Services", ShortName = "SSERV" });
            departments.Add(new Department() { Name = "Other", ShortName = "Other" });

            foreach (var department in departments)
            {
                department.Categories.Add("Proficiency");
                department.Categories.Add("Service rendered");
                department.Categories.Add("Response time");
                department.Categories.Add("Other");

                switch (department.ShortName)
                {
                    case "ACAD":
                    case "NEP":
                        department.Categories.Add("Course or unit contents");
                        department.Categories.Add("Lecturer proficiency");
                        department.Categories.Add("Lecturer quality of delivery");
                        break;
                    case "LIB":
                        department.Categories.Add("Facilities");
                        department.Categories.Add("Rules and regulations");
                        break;
                    case "SSERV":
                        department.Categories.Add("Enrichment classes");
                        department.Categories.Add("Graduation");
                        department.Categories.Add("Orientation");
                        break;
                    case "MKT":
                        department.Categories.Add("Agents' management");
                        department.Categories.Add("Marketing material");
                        break;
                    case "FIN":
                        department.Categories.Add("Financial matters");
                        department.Categories.Add("Waiting time");
                        break;
                    case "IT":
                        department.Categories.Add("Hardware problem");
                        department.Categories.Add("Support");
                        department.Categories.Add("System problem");
                        break;
                    case "FAC":
                        department.Categories.Add("Adhoc maintenance");
                        department.Categories.Add("Cleanliness");
                        department.Categories.Add("Environment");
                        department.Categories.Add("Facilities adequacy");
                        department.Categories.Add("Parking");
                        department.Categories.Add("Preventive maintenance");
                        department.Categories.Add("Scheduled maintenance");
                        break;
                }
            }
        }

        private static IList<string> GetAllCategories()
        {
            var allCategories = new List<string>();
            foreach (var department in departments)
            {
                foreach (var category in department.Categories)
                {
                    if (allCategories.Exists(c => c == category))
                        break;
                    allCategories.Add(category);
                }
            }

            return allCategories;
        }

        private static void ReadDatabase()
        {
            //var openFileDialog = new Forms.OpenFileDialog
            //{
            //    Filter = "MDB files (*.mdb)|*.mdb"
            //};
            //if (openFileDialog.ShowDialog() != Forms.DialogResult.OK)
            //    return;
            //var databaseLocation = openFileDialog.FileName;
            var databaseLocation = @"C:\Users\weisong.teng\Desktop\Feedback Database.mdb";

            var connectionString = $"Provider=Microsoft.Jet.OLEDB.4.0;Data Source ={databaseLocation};Persist Security Info=True";
            var connection = new OleDbConnection(connectionString);
            connection.Open();

            var sql = "SELECT * FROM Feedbacks";
            //var sql = "SELECT * FROM Feedbacks WHERE Course_or_Department IN ('Diploma of Arts and Creative industries', 'Diploma of Commerce')";
            var command = new OleDbCommand(sql, connection);
            var dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                feedbacks.Add(new Feedback()
                {
                    Id = int.Parse(dataReader["Id"].ToString()),
                    DateOfFeedback = DateTime.Parse(dataReader["Date_Received"].ToString()),
                    CourseOrDepartment = dataReader["Course_or_Department"].ToString(),
                    FeedbackNature = dataReader["Feedback_Nature"].ToString(),
                    DepartmentResponsible = dataReader["Department_Responsible"].ToString(),
                    FeedbackCode = dataReader["Feedback_Code"].ToString(),
                    IsRemoved = dataReader["IsRemoved"].ToString(),
                    IsExcludedFromAnalysis = dataReader["IsExcludedFromAnalysis"].ToString()
                });
            }

            feedbacks = feedbacks.Where(f => f.IsRemoved.ToLower() != "yes").Where(f => f.IsExcludedFromAnalysis != "yes").ToList();

            dataReader.Close();
            connection.Close();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_AllTime_Bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback trend from 2012-Q1 to {endYear}-Q{endQuarter}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.Year == year && f.Quarter == quarter).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_AllTime_Bar_PercentageOnly()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback trend from 2012-Q1 to {endYear}-Q{endQuarter}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();
            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.Year == year && f.Quarter == quarter).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Complaint").Count(), v.Count)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Compliment").Count(), v.Count)).ToArray();
            series2.ApplyDataLabels();
            ((Excel.DataLabels)series2.DataLabels()).NumberFormat = "0%";

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Information").Count(), v.Count)).ToArray();
            series3.ApplyDataLabels();
            ((Excel.DataLabels)series3.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_CurrentYear_Bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback trend in {endYear}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var quarter in quarters)
            {
                var data = feedbacks.Where(f => f.Year == endYear && f.Quarter == quarter).ToList();
                result.Add($"{endYear}-Q{quarter}", data);

                if (quarter == endQuarter)
                    break;
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_CurrentQuarter_Bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature in {endYear}-Q{endQuarter}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback nature";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();
            //result.Add("Total feedbacks", feedbacks.Where(f => f.Year == endYear && f.Quarter == endQuarter).ToList());
            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackNature == feedbackNature)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .ToList();
                result.Add(feedbackNature, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsDepartment_AllTime_Bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback per department from 2012-Q1 to {endYear}-Q{endQuarter}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).ToList();
                result.Add(department.Name, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_AllTime_Bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature from 2012-Q1 to {endYear}-{endQuarter} for {department.Name}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).Where(f => f.Year == year && f.Quarter == quarter).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_CurrentYear_Bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature in {endYear} for {department.Name}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var quarter in quarters)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).Where(f => f.Year == endYear && f.Quarter == quarter).ToList();
                result.Add($"{endYear}-Q{quarter}", data);

                if (quarter == endQuarter)
                    break;
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackVsQuarter_allTime_bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback all time trend (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.Year == year && f.Quarter == quarter).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count).ToArray();
            series1.ApplyDataLabels();

            var trendlines = (Excel.Trendlines)series1.Trendlines();
            var trendline1 = trendlines.Add(Excel.XlTrendlineType.xlLinear);
            trendline1.Border.Color = series1.Format.Fill.ForeColor;
            trendline1.Name = "Trendline";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_allTime_line()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlLine;
            chart.ChartTitle.Text = $"Feedback nature trend up to {endYear}-Q{endQuarter} (line)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.Year == year && f.Quarter == quarter).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_FeedbackNature_bar(string feedbackNature)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback categories for {feedbackNature} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback categories";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in categories)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackCode == category)
                    .Where(f => f.FeedbackNature == feedbackNature)
                    .ToList();

                result.Add($"{category}", data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Categories";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryAndFeedbackNature_Department_AllTime_Bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback categories from 2012-Q1 to {endYear}-Q{endQuarter} for {department.Name}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback categories";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in department.Categories)
            {
                var data = feedbacks
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .Where(f => f.FeedbackCode == category)
                    .ToList();
                result.Add($"{category}", data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_FeedbackNature_AllTime_Bar(string feedbackNature, string category)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback on {category.ToLower()} for {feedbackNature.ToLower()} from 2012-Q1 to {endYear}-Q{endQuarter}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback categories";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks
                        .Where(f => f.FeedbackCode == category)
                        .Where(f => f.FeedbackNature == feedbackNature)
                        .Where(f => f.Year == year && f.Quarter == quarter)
                        .ToList();

                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = $"{feedbackNature} {category}";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryVsQuarter_Department_CurrentYear_Bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category for {endYear} for {department.Name}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var quarter in quarters)
            {
                var data = feedbacks
                    .Where(f => f.Year == endYear && f.Quarter == quarter)
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .ToList();
                result.Add($"{endYear}-Q{quarter}", data);

                if (quarter == endQuarter)
                    break;
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            foreach (var category in department.Categories)
            {
                var series = seriesCollection.NewSeries();
                series.Name = category;
                series.XValues = result.Keys.ToArray();
                series.Values = result.Values.Select(v => v.Where(f => f.FeedbackCode == category).Count()).ToArray();
                series.ApplyDataLabels();
            }

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_Department_CurrentQuarter_Bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category in {endYear}-Q{endQuarter} for {department.Name}";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Categories";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in department.Categories)
            {
                var data = feedbacks
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .Where(f => f.FeedbackCode == category)
                    .ToList();
                result.Add(category, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series = seriesCollection.NewSeries();
            series.Name = "Categories";
            series.XValues = result.Keys.ToArray();
            series.Values = result.Values.Select(v => v.Count()).ToArray();
            series.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_FeedbackNature_percentageOnly_bar(string feedbackNature)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback categories for {feedbackNature} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback categories";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in categories)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackCode == category)
                    .Where(f => f.FeedbackNature == feedbackNature)
                    .ToList();

                result.Add($"{category}", data);
            }

            var totalFeedbackCount = feedbacks.Count;

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Categories";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Count(), totalFeedbackCount)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_allTime_bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback nature";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks.Where(f => f.FeedbackNature == feedbackNature).ToList();
                result.Add(feedbackNature, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_currentQuarter_bar_percentageOnly()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature {endYear}-Q{endQuarter} in percentage (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback nature";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();
            result.Add("Total feedbacks", feedbacks.Where(f => f.Year == endYear && f.Quarter == endQuarter).ToList());
            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackNature == feedbackNature)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .ToList();
                result.Add(feedbackNature, data);
            }

            var totalFeedbackCount = feedbacks
                .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                .Count();

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Count, totalFeedbackCount)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_allTime_pie()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlPie;
            chart.ChartTitle.Text = $"Feedback nature (pie)";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks.Where(f => f.FeedbackNature == feedbackNature).ToList();
                result.Add(feedbackNature, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_currentQuarter_pie()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlPie;
            chart.ChartTitle.Text = $"Feedback nature {endYear}-Q{endQuarter} (pie)";

            var result = new Dictionary<string, IList<Feedback>>();
            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks.Where(f => f.FeedbackNature == feedbackNature).Where(f => f.Year == endYear && f.Quarter == endQuarter).ToList();
                result.Add(feedbackNature, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels(ShowPercentage: true);

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackVsDepartment_allTime_bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback per department (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).ToList();
                result.Add(department.ShortName, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsDepartment_allTime_line()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlLine;
            chart.ChartTitle.Text = $"Feedback nature trend per department up to {endYear}-Q{endQuarter} (line)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).ToList();
                result.Add(department.ShortName, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            var series0 = seriesCollection.NewSeries();
            series0.Name = "Total feedbacks";
            series0.XValues = result.Keys.ToArray();
            series0.Values = result.Values.Select(v => v.Count()).ToArray();
            series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsDepartment_allTime_bar_percentageOnly()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature trend per department up to {endYear}-Q{endQuarter} in percentage (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).ToList();
                result.Add(department.ShortName, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Complaint").Count(), v.Count)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Compliment").Count(), v.Count)).ToArray();
            series2.ApplyDataLabels();
            ((Excel.DataLabels)series2.DataLabels()).NumberFormat = "0%";

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Information").Count(), v.Count)).ToArray();
            series3.ApplyDataLabels();
            ((Excel.DataLabels)series3.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsDepartment_currentQuarter_bar()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature per department {endYear}-Q{endQuarter} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).Where(f => f.Year == endYear && f.Quarter == endQuarter).ToList();
                result.Add(department.Name, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            //var series0 = seriesCollection.NewSeries();
            //series0.Name = "Total feedbacks";
            //series0.XValues = result.Keys.ToArray();
            //series0.Values = result.Values.Select(v => v.Count()).ToArray();
            //series0.ApplyDataLabels();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsDepartment_currentQuarter_bar_percentageOnly()
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature per department {endYear}-Q{endQuarter} in percentage (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Department";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var department in departments)
            {
                var data = feedbacks.Where(f => f.DepartmentResponsible == department.Name).Where(f => f.Year == endYear && f.Quarter == endQuarter).ToList();
                result.Add(department.ShortName, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Complaint").Count(), v.Count)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Compliment").Count(), v.Count)).ToArray();
            series2.ApplyDataLabels();
            ((Excel.DataLabels)series2.DataLabels()).NumberFormat = "0%";

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackNature == "Information").Count(), v.Count)).ToArray();
            series3.ApplyDataLabels();
            ((Excel.DataLabels)series3.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNatureVsQuarter_department_allTime_bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback nature all time trend for {department.ShortName} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks.Where(f => f.Year == year && f.Quarter == quarter).Where(f => f.DepartmentResponsible == department.Name).ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            var trendlines = (Excel.Trendlines)series1.Trendlines();
            var trendline1 = trendlines.Add(Excel.XlTrendlineType.xlLinear);
            trendline1.Border.Color = series1.Format.Fill.ForeColor;
            trendline1.Name = "Trendline";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackNature_currentQuarter_department_pie(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlPie;
            chart.ChartTitle.Text = $"Feedback nature {endYear}-Q{endQuarter} for {department.ShortName} (pie)";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var feedbackNature in feedbackNatures)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackNature == feedbackNature)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .ToList();
                result.Add(feedbackNature, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_currentQuarter_department_pie(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlPie;
            chart.ChartTitle.Text = $"Feedback category {endYear}-Q{endQuarter} for {department.ShortName} (pie)";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in department.Categories)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackCode == category)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .ToList();
                result.Add(category, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_currentQuarter_department_bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category {endYear}-Q{endQuarter} for {department.Name} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback category";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var category in department.Categories)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackCode == category)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .ToList();
                result.Add(category, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Count()).ToArray();
            series1.ApplyDataLabels();

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategory_currentQuarter_department_bar_percentageOnly(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category {endYear}-Q{endQuarter} for {department.ShortName} in percentage (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Feedback category";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var result = new Dictionary<string, IList<Feedback>>();
            var totalFeedbackCount = feedbacks
                .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                .Where(f => f.DepartmentResponsible == department.Name)
                .Count();

            foreach (var category in department.Categories)
            {
                var data = feedbacks
                    .Where(f => f.FeedbackCode == category)
                    .Where(f => f.Year == endYear && f.Quarter == endQuarter)
                    .Where(f => f.DepartmentResponsible == department.Name)
                    .ToList();
                result.Add(category, data);
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Feedbacks";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => GetRatio(v.Count(), totalFeedbackCount)).ToArray();
            series1.ApplyDataLabels();
            ((Excel.DataLabels)series1.DataLabels()).NumberFormat = "0%";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryVsQuarter_department_allTime_line(Department department, string category)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlLine;
            chart.ChartTitle.Text = $"Feedback category trend up to {endYear}-Q{endQuarter} for {department.ShortName} on {category} (line)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks
                        .Where(f => f.Year == year && f.Quarter == quarter)
                        .Where(f => f.DepartmentResponsible == department.Name)
                        .Where(f => f.FeedbackCode == category)
                        .ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();
            var series1 = seriesCollection.NewSeries();
            series1.Name = "Complaints";
            series1.XValues = result.Keys.ToArray();
            series1.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Complaint").Count()).ToArray();
            series1.ApplyDataLabels();

            var series2 = seriesCollection.NewSeries();
            series2.Name = "Compliments";
            series2.XValues = result.Keys.ToArray();
            series2.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Compliment").Count()).ToArray();
            series2.ApplyDataLabels();

            var series3 = seriesCollection.NewSeries();
            series3.Name = "For information";
            series3.XValues = result.Keys.ToArray();
            series3.Values = result.Values.Select(v => v.Where(f => f.FeedbackNature == "Information").Count()).ToArray();
            series3.ApplyDataLabels();

            var trendlines = (Excel.Trendlines)series1.Trendlines();
            var trendline1 = trendlines.Add(Excel.XlTrendlineType.xlLinear);
            trendline1.Border.Color = series1.Format.Fill.ForeColor;
            trendline1.Name = "Trendline";

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryVsQuarter_department_allTime_bar(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category trend up to {endYear}-Q{endQuarter} for {department.Name} (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var categories = department.Categories.ToList();

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks
                        .Where(f => f.Year == year && f.Quarter == quarter)
                        .Where(f => f.DepartmentResponsible == department.Name)
                        .ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            foreach (var category in categories)
            {
                var series = seriesCollection.NewSeries();
                series.Name = category;
                series.XValues = result.Keys.ToArray();
                series.Values = result.Values.Select(v => v.Where(f => f.FeedbackCode == category).Count()).ToArray();
                series.ApplyDataLabels();
            }

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryVsQuarter_department_allTime_line(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlLine;
            chart.ChartTitle.Text = $"Feedback category trend up to {endYear}-Q{endQuarter} for {department.ShortName} (line)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";

            var categories = department.Categories.ToList();

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks
                        .Where(f => f.Year == year && f.Quarter == quarter)
                        .Where(f => f.DepartmentResponsible == department.Name)
                        .ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            foreach (var category in categories)
            {
                var series = seriesCollection.NewSeries();
                series.Name = category;
                series.XValues = result.Keys.ToArray();
                series.Values = result.Values.Select(v => v.Where(f => f.FeedbackCode == category).Count()).ToArray();
                series.ApplyDataLabels();
            }

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static void CreateChart_FeedbackCategoryVsQuarter_department_allTime_bar_percentageOnly(Department department)
        {
            var chart = (Excel.Chart)workbook.Charts.Add();
            chart.ApplyDataLabels();
            chart.HasTitle = true;
            chart.HasLegend = true;
            chart.ChartType = Excel.XlChartType.xlColumnClustered;
            chart.ChartTitle.Text = $"Feedback category trend up to {endYear}-Q{endQuarter} for {department.ShortName} in percentage (bar)";

            var xAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            xAxis.HasTitle = true;
            xAxis.AxisTitle.Text = "Year-quarter";
            var yAxis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlValue, Excel.XlAxisGroup.xlPrimary);
            yAxis.HasTitle = true;
            yAxis.AxisTitle.Text = "Number of feedback";
            yAxis.TickLabels.NumberFormat = "0%";

            var categories = department.Categories.ToList();

            var result = new Dictionary<string, IList<Feedback>>();

            foreach (var year in years)
            {
                foreach (var quarter in quarters)
                {
                    var data = feedbacks
                        .Where(f => f.Year == year && f.Quarter == quarter)
                        .Where(f => f.DepartmentResponsible == department.Name)
                        .ToList();
                    result.Add($"{year}-Q{quarter}", data);

                    if (year == endYear && quarter == endQuarter)
                        break;
                }
            }

            var seriesCollection = (Excel.SeriesCollection)chart.SeriesCollection();

            foreach (var category in categories)
            {
                var series = seriesCollection.NewSeries();
                series.Name = category;
                series.XValues = result.Keys.ToArray();
                series.Values = result.Values.Select(v => GetRatio(v.Where(f => f.FeedbackCode == category).Count(), v.Count)).ToArray();
                series.ApplyDataLabels();
                ((Excel.DataLabels)series.DataLabels()).NumberFormat = "0%";
            }

            chart.Export($@"{exportPath}{chartNumber} - {chart.ChartTitle.Text}.png", "PNG");
            chartNumber++;
            chart.Delete();
        }

        private static double GetRatio(int value, int total)
        {
            var result = (double)value / total;

            return (total == 0) ? 0 : result;
        }
    }
}
