﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class FeedbackNature
    {
        public const string Complaint = "Complaint";
        public const string Compliment = "Compliment";
        public const string Information = "Information";
    }
}
