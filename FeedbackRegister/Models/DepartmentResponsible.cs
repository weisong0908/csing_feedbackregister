﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class ResponsibleDepartment
    {
        public const string Academic = "Academic";
        public const string Admissions = "Admissions";
        public const string Facilities = "Facilities";
        public const string Finance = "Finance";
        public const string HumanResourceAdministration = "Human Resource and Administration";
        public const string ITSupport = "IT Support";
        public const string Library = "Library";
        public const string Marketing = "Marketing";
        public const string NavitasEnglish = "Navitas English";
        public const string QualityCompliance = "Quality and Compliance";
        public const string StudentAdministration = "Student Administration";
        public const string StudentCentral = "Student Central";
        public const string StudentServices = "Student Services";
        public const string Other = "Other";
    }
}
