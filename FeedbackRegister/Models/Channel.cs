﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class Channel
    {
        public const string FeedbackForm = "Feedback form";
        public const string Email = "Email";
        public const string PhoneCall = "Phone call";
    }
}
