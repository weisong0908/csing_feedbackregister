﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class CourseOrDepartment
    {
        public const string BA_MC = "Bachelor of Arts (Mass Communication)";
        public const string BCOM_AF = "Bachelor of Commerce (Accounting and Finance)";
        public const string BCOM_AC = "Bachelor of Commerce (Accounting)";
        public const string BCOM_BF = "Bachelor of Commerce (Banking and Finance)";
        public const string BCOM_FM = "Bachelor of Commerce (Finance and Marketing)";
        public const string BCOM_IB = "Bachelor of Commerce (International Business)";
        public const string BCOM_LSCM = "Bachelor of Commerce (Logistics and Supply Chain Management)";
        public const string BCOM_MHRM = "Bachelor of Commerce (Management and Human Resource Management)";
        public const string BCOM_MM = "Bachelor of Commerce (Management and Marketing)";
        public const string BCOM_MA = "Bachelor of Commerce (Marketing and Advertising)";
        public const string BCOM_MPR = "Bachelor of Commerce (Marketing and Public Relations)";
        public const string BCOM_M = "Bachelor of Commerce (Marketing)";
        public const string BSN = "Bachelor of Science (Nursing) Conversion Program For Registered Nurses (Top-Up)";
        public const string CENG_E = "Certificate in General English (Elementary)";
        public const string CENG_P = "Certificate in General English (Pre-intermediate)";
        public const string DACI = "Diploma of Arts and Creative industries";
        public const string DCOM = "Diploma of Commerce";
        public const string DEAP = "Diploma of English For Academic Purposes";
        public const string GC_CN = "Graduate Certificate in Clinical Nursing";
        public const string GC_WOCP = "Clinical Nursing to Graduate Certificate in Wound, Ostomy and Continence Practice";
        public const string GC_OHSM = "Graduate Certificate in Occupational Health and Safety Management";
        public const string GC_PM = "Graduate Certificate in Project Management";
        public const string GD_CN = "Graduate Diploma in Clinical Nursing";
        public const string GD_WOCP = "Graduate Diploma in Wound, Ostomy and Continence Practice";
        public const string GD_IB = "Graduate Diploma in International Business";
        public const string GD_PM = "Graduate Diploma in Project Management";
        public const string MBA = "Master of Business Administration (Global)";
        public const string MIB = "Master of International Business";
        public const string MOHS_OHS = "Master of Occupational Health and Safety";
        public const string MS_CL = "Master of Science (Clinical Leadership)";
        public const string MS_CN = "Master of Science (Clinical Nursing)";
        public const string MS_HP = "Master of Science (Health Practice)";
        public const string MS_PM = "Master of Science (Project Management)";
        public const string MSCM = "Master of Supply Chain Management";
        public const string PD_OHS = "Postgraduate Diploma in Occupational Health and Safety";
        public const string SA = "Study Abroad Business";
        public const string NFD = "Not for Degree";

        public const string Academic = "Academic";
        public const string Admissions = "Admissions";
        public const string Facilities = "Facilities";
        public const string Finance = "Finance";
        public const string HumanResourceAdministration = "Human Resource and Administration";
        public const string ITSupport = "IT Support";
        public const string Library = "Library";
        public const string Marketing = "Marketing";
        public const string NavitasEnglish = "Navitas English";
        public const string QualityCompliance = "Quality and Compliance";
        public const string StudentAdministration = "Student Administration";
        public const string StudentCentral = "Student Central";
        public const string StudentServices = "Student Services";

        public const string Lecturer = "Lecturer";
        public const string ProspectiveStudent = "Prospective student";
        public const string MemberOfPublic = "Member of public";
        public const string GovernmentAgencyOrNGO = "Government agency or NGO";
        
        public const string Other = "Other";



    }
}
