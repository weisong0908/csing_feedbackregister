﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class Category
    {
        public const string Proficiency = "Proficiency";
        public const string ServiceRendered = "Service rendered";
        public const string ResponseTime = "Response time";
        public const string CourseOrUnitContents = "Course or unit contents";
        public const string LecturerProficiency = "Lecturer proficiency";
        public const string LecturerQualityOfDelivery = "Lecturer quality of delivery";
        public const string Facilities = "Facilities";
        public const string RulesAndRegulations = "Rules and regulations";
        public const string EnrichmentClasses = "Enrichment classes";
        public const string Graduation = "Graduation";
        public const string Orientation = "Orientation";
        public const string AgentsManagement = "Agents' management";
        public const string MarketingMaterial = "Marketing material";
        public const string FinancialMatters = "Financial matters";
        public const string WaitingTime = "Waiting time";
        public const string HardwareProblem = "Hardware problem";
        public const string Support = "Support";
        public const string SystemProblem = "System problem";
        public const string AdhocMaintenance = "Adhoc maintenance";
        public const string Cleanliness = "Cleanliness";
        public const string Environment = "Environment";
        public const string FacilitiesAdequacy = "Facilities adequacy";
        public const string Parking = "Parking";
        public const string PreventiveMaintenance = "Preventive maintenance";
        public const string ScheduledMaintenance = "Scheduled maintenance";
        public const string Other = "Other";        
    }
}
