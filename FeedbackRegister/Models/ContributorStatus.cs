﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public static class ContributorStatus
    {
        public const string Student = "Student";
        public const string Staff = "Staff";
        public const string OtherStakeholder = "Other stakeholder";
    }
}
