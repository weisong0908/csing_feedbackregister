﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.Models
{
    public class Update
    {
        public int FeedbackId { get; set; }
        public string Field { get; set; }
        public string Author { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public DateTime DateUpdate { get; set; }
    }
}
