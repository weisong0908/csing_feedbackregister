﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using FeedbackRegister.Models;

namespace FeedbackRegister.Persistence
{
    public class UpdateStore : IUpdateStore
    {
        private OleDbConnection _dbConnection;
        private OleDbCommand _command;

        public UpdateStore(string connectionString)
        {
            _dbConnection = new OleDbConnection(connectionString);
        }

        public void AddUpdate(IList<Update> updates)
        {
            _dbConnection.Open();

            foreach (Update update in updates)
            {
                string sql = "INSERT INTO Updates " +
                    "(Feedback_Id, Field, Author, Old_Value, New_Value, Date_Update) VALUES " +
                    "(@FeedbackId, @Field, @Author, @OldValue, @NewValue, @DateUpdate)";
                _command = new OleDbCommand(sql, _dbConnection);

                OleDbParameter FeedbackId = new OleDbParameter("@FeedbackId", update.FeedbackId);
                OleDbParameter Field = new OleDbParameter("@Field", update.Field);
                OleDbParameter Author = new OleDbParameter("@Author", update.Author);
                OleDbParameter OldValue = new OleDbParameter("@OldValue", update.OldValue);
                OleDbParameter NewValue = new OleDbParameter("@NewValue", update.NewValue);
                OleDbParameter DateUpdate = new OleDbParameter("@DateUpdate", update.DateUpdate.ToString());

                _command.Parameters.Add(FeedbackId);
                _command.Parameters.Add(Field);
                _command.Parameters.Add(Author);
                _command.Parameters.Add(OldValue);
                _command.Parameters.Add(NewValue);
                _command.Parameters.Add(DateUpdate);

                _command.ExecuteNonQuery();
            }

            _dbConnection.Close();
        }
    }
}
