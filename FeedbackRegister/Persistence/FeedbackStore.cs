﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FeedbackRegister.Models;
using System.Data.OleDb;

namespace FeedbackRegister.Persistence
{
    public class FeedbackStore : IFeedbackStore
    {
        private OleDbConnection _dbConnection;
        private OleDbDataReader _dataReader;
        private OleDbCommand _command;
        
        public FeedbackStore(string connectionString)
        {
            _dbConnection = new OleDbConnection(connectionString);
        }

        public IEnumerable<Feedback> GetFeedbacks()
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT Id, Date_Received, Feedback_Status, Contributor_Name, IsRemoved FROM Feedbacks", _dbConnection);
            _dataReader = _command.ExecuteReader();

            var feedbacks = new List<Feedback>();

            while (_dataReader.Read())
            {
                feedbacks.Add(new Feedback()
                {
                    Id = int.Parse(_dataReader["Id"].ToString()),
                    DateReceived = (_dataReader["Date_Received"] == DBNull.Value) ? DateTime.Today : DateTime.Parse(_dataReader["Date_Received"].ToString()),
                    Progress = ReadString(_dataReader["Feedback_Status"]),
                    ContributorName = ReadString(_dataReader["Contributor_Name"]),
                    IsRemoved = ReadString(_dataReader["IsRemoved"])
                });
            }

            _dataReader.Close();
            _dbConnection.Close();

            return feedbacks;
        }

        private string ReadString(object dbValue)
        {
            if (dbValue == DBNull.Value)
                return string.Empty;

            return dbValue.ToString();
        }

        public Feedback GetFeedback(int Id)
        {
            _dbConnection.Open();
            _command = new OleDbCommand("SELECT * FROM Feedbacks WHERE Id =" + Id, _dbConnection);
            _dataReader = _command.ExecuteReader();

            Feedback feedback = null;

            while (_dataReader.Read())
            {
                feedback = new Feedback()
                {
                    Id = int.Parse(_dataReader["Id"].ToString()),
                    DateReceived = (_dataReader["Date_Received"] == DBNull.Value) ? DateTime.Today : DateTime.Parse(_dataReader["Date_Received"].ToString()),
                    Channel = ReadString(_dataReader["Channel"]),
                    Progress = ReadString(_dataReader["Feedback_Status"]),
                    FeedbackNature = ReadString(_dataReader["Feedback_Nature"]),
                    DateAcknowledged = (_dataReader["Date_Acknowledgement"] == DBNull.Value) ? DateTime.MinValue : DateTime.Parse(_dataReader["Date_Acknowledgement"].ToString()),
                    ResponsibleDepartment = ReadString(_dataReader["Department_Responsible"]),
                    ContributorName = ReadString(_dataReader["Contributor_Name"]),
                    StudentId = ReadString(_dataReader["Student_ID"]),
                    ContributorStatus = ReadString(_dataReader["Contributor_Status"]),
                    CourseOrDepartment = ReadString(_dataReader["Course_or_Department"]),
                    Phone = ReadString(_dataReader["Contact_No"]),
                    Email = ReadString(_dataReader["Email"]),
                    Title = ReadString(_dataReader["Title"]),
                    FeedbackSummary = ReadString(_dataReader["Feedback_Summary"]),
                    ActionBy = ReadString(_dataReader["Action_By"]),
                    ApprovedBy = ReadString(_dataReader["Approved_By"]),
                    RectificationSummary = ReadString(_dataReader["Rectification_Summary"]),
                    DateResolved = (_dataReader["Date_Resolved"] == DBNull.Value) ? DateTime.MinValue : DateTime.Parse(_dataReader["Date_Resolved"].ToString()),
                    Category = ReadString(_dataReader["Feedback_Code"]),
                    Remarks = ReadString(_dataReader["Remarks"]),
                    IsRemoved = ReadString(_dataReader["IsRemoved"])
                };
            }

            _dataReader.Close();
            _dbConnection.Close();

            if (feedback == null)
                return new Feedback();

            return feedback;
        }

        public void AddFeedback(Feedback feedback)
        {
            string sql = "INSERT INTO Feedbacks " +
                "(Date_Received, Contributor_Name, Student_ID, Contributor_Status, Course_or_Department, Contact_No, " +
                "Email, Feedback_Nature, Channel, Title, Feedback_Summary, Date_Acknowledgement, Department_Responsible, Action_By, Approved_By, Rectification_Summary, Date_Resolved, Feedback_Status, " +
                "Feedback_Code, Remarks) VALUES " +
                "(@DateReceived, @ContributorName, @StudentId, @ContributorStatus, @CourseOrDepartment, @Phone, " +
                "@Email, @FeedbackNature, @Channel, @Title, @FeedbackSummary, @DateAcknowledged, @ResponsibleDepartment, @ActionBy, @ApprovedBy, @RectificationSummary, @DateResolved, @Progress, " +
                "@Category, @Remarks)";

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter DateReceived = new OleDbParameter("@DateReceived", feedback.DateReceived.ToShortDateString());
            OleDbParameter ContributorName = new OleDbParameter("@ContributorName", feedback.ContributorName);
            OleDbParameter StudentId = new OleDbParameter("@StudentId", feedback.StudentId);
            OleDbParameter ContributorStatus = new OleDbParameter("@ContributorStatus", feedback.ContributorStatus);
            OleDbParameter CourseOrDepartment = new OleDbParameter("@CourseOrDepartment", feedback.CourseOrDepartment);
            OleDbParameter Phone = new OleDbParameter("@Phone", feedback.Phone);
            OleDbParameter Email = new OleDbParameter("@Email", feedback.Email);
            OleDbParameter FeedbackNature = new OleDbParameter("@FeedbackNature", feedback.FeedbackNature);
            OleDbParameter Channel = new OleDbParameter("@Channel", feedback.Channel);
            OleDbParameter Title = new OleDbParameter("@Title", feedback.Title);
            OleDbParameter FeedbackSummary = new OleDbParameter("@FeedbackSummary", feedback.FeedbackSummary);
            OleDbParameter DateAcknowledged = new OleDbParameter("@DateAcknowledged", feedback.DateAcknowledged.Value.ToShortDateString());
            OleDbParameter ResponsibleDepartment = new OleDbParameter("@ResponsibleDepartment", feedback.ResponsibleDepartment);
            OleDbParameter ActionBy = new OleDbParameter("@ActionBy", feedback.ActionBy);
            OleDbParameter ApprovedBy = new OleDbParameter("@ApprovedBy", feedback.ApprovedBy);
            OleDbParameter RectificationSummary = new OleDbParameter("@RectificationSummary", feedback.RectificationSummary);
            OleDbParameter DateResolved = new OleDbParameter("@DateResolved", feedback.DateResolved.Value.ToShortDateString());
            OleDbParameter Progress = new OleDbParameter("@Progress", feedback.Progress);
            OleDbParameter Category = new OleDbParameter("@Category", feedback.Category);
            OleDbParameter Remarks = new OleDbParameter("@Remarks", feedback.Remarks);

            _command.Parameters.Add(DateReceived);
            _command.Parameters.Add(ContributorName);
            _command.Parameters.Add(StudentId);
            _command.Parameters.Add(ContributorStatus);
            _command.Parameters.Add(CourseOrDepartment);
            _command.Parameters.Add(Phone);
            _command.Parameters.Add(Email);
            _command.Parameters.Add(FeedbackNature);
            _command.Parameters.Add(Channel);
            _command.Parameters.Add(Title);
            _command.Parameters.Add(FeedbackSummary);
            _command.Parameters.Add(DateAcknowledged);
            _command.Parameters.Add(ResponsibleDepartment);
            _command.Parameters.Add(ActionBy);
            _command.Parameters.Add(ApprovedBy);
            _command.Parameters.Add(RectificationSummary);
            _command.Parameters.Add(DateResolved);
            _command.Parameters.Add(Progress);
            _command.Parameters.Add(Category);
            _command.Parameters.Add(Remarks);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }

        public void RemoveFeedback(Feedback feedback)
        {
            string sql = "UPDATE Feedbacks SET IsRemoved = @IsRemoved WHERE Id = " + feedback.Id.ToString();

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter IsRemoved = new OleDbParameter("@IsRemoved", feedback.IsRemoved);
            _command.Parameters.Add(IsRemoved);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }

        public void UpdateFeedback(Feedback feedback)
        {
            string sql = "UPDATE Feedbacks SET " +
                "Date_Received = @DateReceived, " +
                "Contributor_Name = @ContributorName, " +
                "Student_ID = @StudentId, " +
                "Contributor_Status = @ContributorStatus, " +
                "Course_or_Department = @CourseOrDepartment, " +
                "Contact_No = @Phone, " +
                "Email = @Email, " +
                "Feedback_Nature = @FeedbackNature, " +
                "Channel = @Channel, " +
                "Title = @Title, " +
                "Feedback_Summary = @FeedbackSummary, " +
                "Date_Acknowledgement = @DateAcknowledged, " +
                "Department_Responsible = @ResponsibleDepartment, " +
                "Rectification_Summary = @RectificationSummary, " +
                "Action_By = @ActionBy, " +
                "Approved_By = @ApprovedBy, " +
                "Date_Resolved = @DateResolved, " +
                "Feedback_Status = @Progress, " +
                "Feedback_Code = @Category, " +
                "Remarks = @Remarks " +
                "WHERE Id = " + feedback.Id.ToString();

            _dbConnection.Open();
            _command = new OleDbCommand(sql, _dbConnection);

            OleDbParameter DateReceived = new OleDbParameter("@DateReceived", feedback.DateReceived.ToShortDateString());
            OleDbParameter ContributorName = new OleDbParameter("@ContributorName", feedback.ContributorName);
            OleDbParameter StudentId = new OleDbParameter("@StudentId", feedback.StudentId);
            OleDbParameter ContributorStatus = new OleDbParameter("@ContributorStatus", feedback.ContributorStatus);
            OleDbParameter CourseOrDepartment = new OleDbParameter("@CourseOrDepartment", feedback.CourseOrDepartment);
            OleDbParameter Phone = new OleDbParameter("@Phone", feedback.Phone);
            OleDbParameter Email = new OleDbParameter("@Email", feedback.Email);
            OleDbParameter FeedbackNature = new OleDbParameter("@FeedbackNature", feedback.FeedbackNature);
            OleDbParameter Channel = new OleDbParameter("@Channel", feedback.Channel);
            OleDbParameter Title = new OleDbParameter("@Title", feedback.Title);
            OleDbParameter FeedbackSummary = new OleDbParameter("@FeedbackSummary", feedback.FeedbackSummary);
            OleDbParameter DateAcknowledged = new OleDbParameter("@DateAcknowledged", feedback.DateAcknowledged.Value.ToShortDateString());
            OleDbParameter ResponsibleDepartment = new OleDbParameter("@ResponsibleDepartment", feedback.ResponsibleDepartment);
            OleDbParameter RectificationSummary = new OleDbParameter("@RectificationSummary", feedback.RectificationSummary);
            OleDbParameter ActionBy = new OleDbParameter("@ActionBy", feedback.ActionBy);
            OleDbParameter ApprovedBy = new OleDbParameter("@ApprovedBy", feedback.ApprovedBy);
            OleDbParameter DateResolved = new OleDbParameter("@DateResolved", feedback.DateResolved.Value.ToShortDateString());
            OleDbParameter Progress = new OleDbParameter("@Progress", feedback.Progress);
            OleDbParameter Category = new OleDbParameter("@Category", feedback.Category);
            OleDbParameter Remarks = new OleDbParameter("@Remarks", feedback.Remarks);

            _command.Parameters.Add(DateReceived);
            _command.Parameters.Add(ContributorName);
            _command.Parameters.Add(StudentId);
            _command.Parameters.Add(ContributorStatus);
            _command.Parameters.Add(CourseOrDepartment);
            _command.Parameters.Add(Phone);
            _command.Parameters.Add(Email);
            _command.Parameters.Add(FeedbackNature);
            _command.Parameters.Add(Channel);
            _command.Parameters.Add(Title);
            _command.Parameters.Add(FeedbackSummary);
            _command.Parameters.Add(DateAcknowledged);
            _command.Parameters.Add(ResponsibleDepartment);
            _command.Parameters.Add(RectificationSummary);
            _command.Parameters.Add(ActionBy);
            _command.Parameters.Add(ApprovedBy);
            _command.Parameters.Add(DateResolved);
            _command.Parameters.Add(Progress);
            _command.Parameters.Add(Category);
            _command.Parameters.Add(Remarks);

            _command.ExecuteNonQuery();
            _dbConnection.Close();
        }
    }
}
