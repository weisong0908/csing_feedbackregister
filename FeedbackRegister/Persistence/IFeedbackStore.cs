﻿using FeedbackRegister.Models;
using System.Collections.Generic;

namespace FeedbackRegister.Persistence
{
    public interface IFeedbackStore
    {
        IEnumerable<Feedback> GetFeedbacks();
        Feedback GetFeedback(int Id);
        void AddFeedback(Feedback feedback);
        void UpdateFeedback(Feedback feedback);
        void RemoveFeedback(Feedback feedback);
    }
}