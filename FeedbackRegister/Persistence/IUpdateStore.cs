﻿using FeedbackRegister.Models;
using System.Collections.Generic;

namespace FeedbackRegister.Persistence
{
    public interface IUpdateStore
    {
        void AddUpdate(IList<Update> updates);
    }
}