﻿using FeedbackRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FeedbackRegister.ViewModels
{
    public class FeedbackViewModel : BaseViewModel
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { SetValue(ref _id, value, nameof(Id)); }
        }

        private DateTime _dateReceived;
        public DateTime DateReceived
        {
            get { return _dateReceived; }
            set { SetValue(ref _dateReceived, value, nameof(DateReceived)); }
        }

        private string _channel;
        public string Channel
        {
            get { return _channel; }
            set { SetValue(ref _channel, value, nameof(Channel)); }
        }

        private string _progress;
        public string Progress
        {
            get { return _progress; }
            set { SetValue(ref _progress, value, nameof(Progress)); }
        }

        private string _feedbackNature;
        public string FeedbackNature
        {
            get { return _feedbackNature; }
            set { SetValue(ref _feedbackNature, value, nameof(FeedbackNature)); }
        }

        private DateTime? _dateAcknowledged;
        public DateTime? DateAcknowledged
        {
            get { return _dateAcknowledged; }
            set { SetValue(ref _dateAcknowledged, value, nameof(DateAcknowledged)); }
        }

        private string _responsibleDepartment;
        public string ResponsibleDepartment
        {
            get { return _responsibleDepartment; }
            set
            {
                SetValue(ref _responsibleDepartment, value, nameof(ResponsibleDepartment));
                ResponsibleDepartmentChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        private string _contributorName;
        public string ContributorName
        {
            get { return _contributorName; }
            set { SetValue(ref _contributorName, value, nameof(ContributorName)); }
        }

        private string _studentId;
        public string StudentId
        {
            get { return _studentId; }
            set { SetValue(ref _studentId, value, nameof(StudentId)); }
        }

        private string _contributorStatus;
        public string ContributorStatus
        {
            get { return _contributorStatus; }
            set { SetValue(ref _contributorStatus, value, nameof(ContributorStatus)); }
        }

        private string _courseOrDepartment;
        public string CourseOrDepartment
        {
            get { return _courseOrDepartment; }
            set
            {
                SetValue(ref _courseOrDepartment, value, nameof(CourseOrDepartment));
            }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { SetValue(ref _phone, value, nameof(Phone)); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { SetValue(ref _email, value, nameof(Email)); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetValue(ref _title, value, nameof(Title)); }
        }

        private string _feedbackSummary;
        public string FeedbackSummary
        {
            get { return _feedbackSummary; }
            set { SetValue(ref _feedbackSummary, value, nameof(FeedbackSummary)); }
        }

        private string _actionBy;
        public string ActionBy
        {
            get { return _actionBy; }
            set { SetValue(ref _actionBy, value, nameof(ActionBy)); }
        }

        private string _approvedBy;
        public string ApprovedBy
        {
            get { return _approvedBy; }
            set { SetValue(ref _approvedBy, value, nameof(ApprovedBy)); }
        }

        private string _rectificationSummary;
        public string RectificationSummary
        {
            get { return _rectificationSummary; }
            set { SetValue(ref _rectificationSummary, value, nameof(RectificationSummary)); }
        }

        private DateTime? _dateResolved;
        public DateTime? DateResolved
        {
            get { return _dateResolved; }
            set { SetValue(ref _dateResolved, value, nameof(DateResolved)); }
        }

        private string _category;
        public string Category
        {
            get { return _category; }
            set { SetValue(ref _category, value, nameof(Category)); }
        }

        private string _remarks;
        public string Remarks
        {
            get { return _remarks; }
            set { SetValue(ref _remarks, value, nameof(Remarks)); }
        }

        public event EventHandler ResponsibleDepartmentChanged;

        public FeedbackViewModel()
        {
            _dateReceived = DateTime.Today;
            _channel = string.Empty;
            _progress = string.Empty;
            _feedbackNature = string.Empty;
            _dateAcknowledged = DateTime.MinValue;
            _responsibleDepartment = string.Empty;
            _contributorName = string.Empty;
            _studentId = string.Empty;
            _contributorStatus = string.Empty;
            _courseOrDepartment = string.Empty;
            _phone = string.Empty;
            _email = string.Empty;
            _title = string.Empty;
            _feedbackSummary = string.Empty;
            _actionBy = string.Empty;
            _approvedBy = string.Empty;
            _rectificationSummary = string.Empty;
            _dateResolved = DateTime.MinValue;
            _category = string.Empty;
            _remarks = string.Empty;
        }

        public FeedbackViewModel(Feedback feedback)
        {
            MapFeedbackToFeedbackViewModel(feedback);
        }

        public void MapFeedbackToFeedbackViewModel(Feedback feedback)
        {
            _id = feedback.Id;
            _dateReceived = feedback.DateReceived;
            _channel = feedback.Channel;
            _progress = feedback.Progress;
            _feedbackNature = feedback.FeedbackNature;
            _dateAcknowledged = feedback.DateAcknowledged;
            _responsibleDepartment = feedback.ResponsibleDepartment;
            _contributorName = feedback.ContributorName;
            _studentId = feedback.StudentId;
            _contributorStatus = feedback.ContributorStatus;
            _courseOrDepartment = feedback.CourseOrDepartment;
            _phone = feedback.Phone;
            _email = feedback.Email;
            _title = feedback.Title;
            _feedbackSummary = feedback.FeedbackSummary;
            _actionBy = feedback.ActionBy;
            _approvedBy = feedback.ApprovedBy;
            _rectificationSummary = feedback.RectificationSummary;
            _dateResolved = feedback.DateResolved;
            _category = feedback.Category;
            _remarks = feedback.Remarks;
        }
    }
}
