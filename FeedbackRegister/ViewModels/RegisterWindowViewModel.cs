﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using FeedbackRegister.Models;
using FeedbackRegister.Persistence;
using System.Reflection;
using FeedbackRegister.Services;
using System.Windows.Input;

namespace FeedbackRegister.ViewModels
{
    public class RegisterWindowViewModel : BaseViewModel
    {
        private IFeedbackStore _feedbackStore;
        private IUpdateStore _updateStore;
        private IEmailService _emailService;
        private IWindowService _windowService;

        public ICommand NewFeedbackCommand { get; set; }
        public ICommand SaveFeedbackCommand { get; set; }
        public ICommand RemoveFeedbackCommand { get; set; }
        public ICommand SendAcknowledgementEmailCommand { get; set; }
        public ICommand ShowAboutWindowCommand { get; set; }

        public IList<FeedbackViewModel> Feedbacks { get; set; } = new ObservableCollection<FeedbackViewModel>();

        private FeedbackViewModel _selectedFeedback;
        public FeedbackViewModel SelectedFeedback
        {
            get { return _selectedFeedback; }
            set { SetValue(ref _selectedFeedback, value, nameof(SelectedFeedback)); }
        }

        private IList<Update> _updates;

        public string UserName { get; set; } = Environment.UserName;
        private int _numberOfFeedbackCases;

        public int NumberOfFeedbackCases
        {
            get { return _numberOfFeedbackCases; }
            set { SetValue(ref _numberOfFeedbackCases, value, nameof(NumberOfFeedbackCases)); }
        }


        private string _alertMessage;
        public string AlertMessage
        {
            get { return _alertMessage; }
            set { SetValue(ref _alertMessage, value, nameof(AlertMessage)); }
        }

        public IEnumerable<string> Channels { get; set; } = GetAllFields(typeof(Channel));
        public IEnumerable<string> Progresses { get; set; } = GetAllFields(typeof(Progress));
        public IEnumerable<string> FeedbackNatures { get; set; } = GetAllFields(typeof(FeedbackNature));
        public IEnumerable<string> ResponsibleDepartments { get; set; } = GetAllFields(typeof(ResponsibleDepartment));
        public IEnumerable<string> ContributorStatuses { get; set; } = GetAllFields(typeof(ContributorStatus));
        public IEnumerable<string> CoursesOrDepartments { get; set; } = GetAllFields(typeof(CourseOrDepartment));
        private IEnumerable<string> _categories;
        public IEnumerable<string> Categories
        {
            get { return _categories; }
            set { SetValue(ref _categories, value, nameof(Categories)); }
        }

        public RegisterWindowViewModel(IFeedbackStore feedbackStore, IUpdateStore updateStore, IEmailService emailService, IWindowService windowService)
        {
            _feedbackStore = feedbackStore;
            _updateStore = updateStore;
            _emailService = emailService;
            _windowService = windowService;

            NewFeedbackCommand = new Command(CreateNewFeedback);
            SaveFeedbackCommand = new Command(SaveFeedback);
            RemoveFeedbackCommand = new Command(RemoveFeedback);
            SendAcknowledgementEmailCommand = new Command(SendAcknowledgementEmail);
            ShowAboutWindowCommand = new Command(ShowAboutWindow);

            Categories = GetAllFields(typeof(Category));
            UpdateRegisterWindow();

            CreateNewFeedback(null);
        }

        private static List<string> GetAllFields(Type type)
        {
            FieldInfo[] fields = type.GetFields();

            var result = new List<string>();

            foreach (FieldInfo field in fields)
                result.Add(field.GetValue(null).ToString());

            return result;
        }

        private void UpdateCategoriesBasedOnResponsibleDepartment(object sender, EventArgs e)
        {
            var result = new List<string>();
            result.Add(Category.Proficiency);
            result.Add(Category.ServiceRendered);
            result.Add(Category.ResponseTime);
            result.Add(Category.Other);

            switch (_selectedFeedback.ResponsibleDepartment)
            {
                case ResponsibleDepartment.Academic:
                    result.Add(Category.CourseOrUnitContents);
                    result.Add(Category.LecturerProficiency);
                    result.Add(Category.LecturerQualityOfDelivery);
                    break;
                case ResponsibleDepartment.Library:
                    result.Add(Category.Facilities);
                    result.Add(Category.RulesAndRegulations);
                    break;
                case ResponsibleDepartment.StudentServices:
                    result.Add(Category.EnrichmentClasses);
                    result.Add(Category.Graduation);
                    result.Add(Category.Orientation);
                    break;
                case ResponsibleDepartment.Marketing:
                    result.Add(Category.AgentsManagement);
                    result.Add(Category.MarketingMaterial);
                    break;
                case ResponsibleDepartment.Finance:
                    result.Add(Category.FinancialMatters);
                    result.Add(Category.WaitingTime);
                    break;
                case ResponsibleDepartment.ITSupport:
                    result.Add(Category.HardwareProblem);
                    result.Add(Category.Support);
                    result.Add(Category.SystemProblem);
                    break;
                case ResponsibleDepartment.Facilities:
                    result.Add(Category.AdhocMaintenance);
                    result.Add(Category.Cleanliness);
                    result.Add(Category.Environment);
                    result.Add(Category.FacilitiesAdequacy);
                    result.Add(Category.Parking);
                    result.Add(Category.PreventiveMaintenance);
                    result.Add(Category.ScheduledMaintenance);
                    break;
            }

            Categories = result;
        }

        private void UpdateCategoriesBasedOnResponsibleDepartment2(string responsibleDepartment)
        {
            var result = new List<string>();
            result.Add(Category.Proficiency);
            result.Add(Category.ServiceRendered);
            result.Add(Category.ResponseTime);
            result.Add(Category.Other);

            switch (responsibleDepartment)
            {
                case ResponsibleDepartment.Academic:
                    result.Add(Category.CourseOrUnitContents);
                    result.Add(Category.LecturerProficiency);
                    result.Add(Category.LecturerQualityOfDelivery);
                    break;
                case ResponsibleDepartment.Library:
                    result.Add(Category.Facilities);
                    result.Add(Category.RulesAndRegulations);
                    break;
                case ResponsibleDepartment.StudentServices:
                    result.Add(Category.EnrichmentClasses);
                    result.Add(Category.Graduation);
                    result.Add(Category.Orientation);
                    break;
                case ResponsibleDepartment.Marketing:
                    result.Add(Category.AgentsManagement);
                    result.Add(Category.MarketingMaterial);
                    break;
                case ResponsibleDepartment.Finance:
                    result.Add(Category.FinancialMatters);
                    result.Add(Category.WaitingTime);
                    break;
                case ResponsibleDepartment.ITSupport:
                    result.Add(Category.HardwareProblem);
                    result.Add(Category.Support);
                    result.Add(Category.SystemProblem);
                    break;
                case ResponsibleDepartment.Facilities:
                    result.Add(Category.AdhocMaintenance);
                    result.Add(Category.Cleanliness);
                    result.Add(Category.Environment);
                    result.Add(Category.FacilitiesAdequacy);
                    result.Add(Category.Parking);
                    result.Add(Category.PreventiveMaintenance);
                    result.Add(Category.ScheduledMaintenance);
                    break;
            }

            Categories = result;
        }

        private void UpdateRegisterWindow()
        {
            Feedbacks.Clear();

            var feedbacks = _feedbackStore.GetFeedbacks().Where(f => f.IsRemoved != "Yes").OrderByDescending(f => f.DateReceived).ThenByDescending(f => f.ContributorName).ToList();

            if (feedbacks.Count == 0)
                return;

            foreach (var feedback in feedbacks)
                Feedbacks.Add(new FeedbackViewModel(feedback));

            AlertMessage = "All feedback cases have been closed.";
            var numberOfUnresolvedCase = Feedbacks.Where(f => f.Progress != "Closed").Count();
            if (numberOfUnresolvedCase > 0)
                AlertMessage = numberOfUnresolvedCase + " unresolved feedback case(s).";

            NumberOfFeedbackCases = feedbacks.Count;
        }

        public void SelectFeedback(FeedbackViewModel selectedFeedback)
        {
            Categories = GetAllFields(typeof(Category));
            SelectedFeedback = new FeedbackViewModel(_feedbackStore.GetFeedback(selectedFeedback.Id));
            SelectedFeedback.ResponsibleDepartmentChanged += UpdateCategoriesBasedOnResponsibleDepartment;

            UpdateCategoriesBasedOnResponsibleDepartment(this, EventArgs.Empty);
        }

        private void CreateNewFeedback(object parameter)
        {
            SelectedFeedback = new FeedbackViewModel();
            SelectedFeedback.ResponsibleDepartmentChanged += UpdateCategoriesBasedOnResponsibleDepartment;
        }

        private void SaveFeedback(object parameter)
        {
            if (SelectedFeedback == null)
                return;

            if (IsContentUpdated() == true || SelectedFeedback.Id == 0)
            {
                var feedback = new Feedback();
                MapFeedbackViewModelToFeedback(feedback, SelectedFeedback);

                if (feedback.Id == 0)
                {
                    _feedbackStore.AddFeedback(feedback);
                }
                else
                {
                    _feedbackStore.UpdateFeedback(feedback);
                }

                _updateStore.AddUpdate(_updates);

                UpdateRegisterWindow();
            }
        }

        private void RemoveFeedback(object parameter)
        {
            if (SelectedFeedback == null)
                return;

            var feedback = new Feedback();
            MapFeedbackViewModelToFeedback(feedback, SelectedFeedback);
            feedback.IsRemoved = "Yes";

            _feedbackStore.RemoveFeedback(feedback);

            UpdateRegisterWindow();
        }

        private void SendAcknowledgementEmail(object parameter)
        {
            _emailService.SendEmail(SelectedFeedback.Email);
        }

        private void ShowAboutWindow(object parameter)
        {
            _windowService.ShowAboutWindow();
        }
        
        private bool IsContentUpdated()
        {
            _updates = new List<Update>();

            var updatedFeedback = SelectedFeedback;
            var originalFeedback = (SelectedFeedback.Id == 0) ? new FeedbackViewModel() : new FeedbackViewModel(_feedbackStore.GetFeedback(SelectedFeedback.Id));

            PropertyInfo[] propertyInfos = SelectedFeedback.GetType().GetProperties();
            foreach(PropertyInfo propertyInfo in propertyInfos)
            {
                var newValue = (string.IsNullOrEmpty(propertyInfo.GetValue(updatedFeedback, null).ToString())) ? "" : propertyInfo.GetValue(updatedFeedback, null).ToString();
                var oldValue = propertyInfo.GetValue(originalFeedback, null).ToString();

                if (newValue == oldValue)
                    continue;

                _updates.Add(new Update()
                {
                    FeedbackId = SelectedFeedback.Id,
                    Field = propertyInfo.Name,
                    Author = UserName,
                    OldValue = oldValue,
                    NewValue = newValue,
                    DateUpdate = DateTime.Now
                });
            }

            return (_updates.Count > 0) ? true : false;            
        }
        
        private void MapFeedbackViewModelToFeedback(Feedback feedback, FeedbackViewModel feedbackViewModel)
        {
            feedback.Id = feedbackViewModel.Id;
            feedback.DateReceived = feedbackViewModel.DateReceived;
            feedback.Channel = feedbackViewModel.Channel;
            feedback.Progress = feedbackViewModel.Progress;
            feedback.FeedbackNature = feedbackViewModel.FeedbackNature;
            feedback.DateAcknowledged = feedbackViewModel.DateAcknowledged;
            feedback.ResponsibleDepartment = feedbackViewModel.ResponsibleDepartment;
            feedback.ContributorName = feedbackViewModel.ContributorName;
            feedback.StudentId = feedbackViewModel.StudentId;
            feedback.ContributorStatus = feedbackViewModel.ContributorStatus;
            feedback.CourseOrDepartment = feedbackViewModel.CourseOrDepartment;
            feedback.Phone = feedbackViewModel.Phone;
            feedback.Email = feedbackViewModel.Email;
            feedback.Title = feedbackViewModel.Title;
            feedback.FeedbackSummary = feedbackViewModel.FeedbackSummary;
            feedback.ActionBy = feedbackViewModel.ActionBy;
            feedback.ApprovedBy = feedbackViewModel.ApprovedBy;
            feedback.RectificationSummary = feedbackViewModel.RectificationSummary;
            feedback.DateResolved = feedbackViewModel.DateResolved;
            feedback.Category = feedbackViewModel.Category;
            feedback.Remarks = feedbackViewModel.Remarks;
        }
    }
}
