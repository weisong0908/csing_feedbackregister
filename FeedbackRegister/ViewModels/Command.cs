﻿using System;
using System.Windows.Input;
using FeedbackRegister.ViewModels;

namespace FeedbackRegister.ViewModels
{
    internal class Command : ICommand
    {
        private Action<object> _execute;

        public Command(Action<object> execute)
        {
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter = null)
        {
            _execute(parameter);
        }
    }
}