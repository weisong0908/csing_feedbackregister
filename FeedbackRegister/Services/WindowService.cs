﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace FeedbackRegister.Services
{
    public class WindowService : IWindowService
    {
        public void ShowMessageBox(string message, string caption)
        {
            MessageBox.Show(message, caption);
        }

        public void ShowAboutWindow()
        {
            Window aboutWindow = Application.Current.Windows.OfType<AboutWindow>().SingleOrDefault();

            if (aboutWindow == null)
            {
                new AboutWindow().Show();
            }
            else
            {
                aboutWindow.Activate();
            }
        }
    }
}
