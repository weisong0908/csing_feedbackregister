﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Outlook;

namespace FeedbackRegister.Services
{
    public class EmailService : IEmailService
    {
        private Application _outlook;
        private MailItem _mailItem;
        private string _mailTemplateLocation;

        public EmailService()
        {
            _mailTemplateLocation = @"S:\Quality Assurance\#QA & COMPLIANCE Dept Functions#\Feedback Management\Feedback Register\Feedback Acknowledgement Template\Curtin Singapore Feedback Acknowledgement.oft";
        }

        public void SendEmail(string recipients)
        {
            _outlook = new Application();
            //_mailItem = (MailItem)_outlook.CreateItem(OlItemType.olMailItem);
            _mailItem = (MailItem)_outlook.CreateItemFromTemplate(_mailTemplateLocation);

            _mailItem.To += recipients;

            Accounts accounts = _outlook.Session.Accounts;
            foreach (Account account in accounts)
            {
                if (account.SmtpAddress == "Feedback@curtin.edu.sg")
                {
                    _mailItem.Sender = account.CurrentUser.AddressEntry;
                    break;
                }
            }

            _mailItem.Display(false);
        }
    }
}
