﻿namespace FeedbackRegister.Services
{
    public interface IEmailService
    {        
        void SendEmail(string recipients);
    }
}