﻿namespace FeedbackRegister.Services
{
    public interface IWindowService
    {
        void ShowAboutWindow();
        void ShowMessageBox(string message, string caption);
    }
}