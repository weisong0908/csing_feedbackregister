﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FeedbackRegister.ViewModels;
using FeedbackRegister.Models;
using FeedbackRegister.Persistence;
using FeedbackRegister.Services;

namespace FeedbackRegister
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindowViewModel ViewModel
        {
            get { return DataContext as RegisterWindowViewModel; }
            set { DataContext = value; }
        }

        public RegisterWindow()
        {
            InitializeComponent();

            //var testConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\weisong.teng\Desktop\Feedback Database - V1.3.mdb;Persist Security Info=True";
            var connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=\\csing.navitas.local\shared\Documents\Quality Assurance\#QA & COMPLIANCE Dept Functions#\Feedback Management\Feedback Register\Feedback Database - V1.3.mdb;Persist Security Info=True";
            var feedbackStore = new FeedbackStore(connectionString);
            var updateStore = new UpdateStore(connectionString);
            var emailService = new EmailService();
            var windowService = new WindowService();

            ViewModel = new RegisterWindowViewModel(feedbackStore, updateStore, emailService, windowService);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            var selectedItem = e.AddedItems[0] as FeedbackViewModel;
            
            ViewModel.SelectFeedback(selectedItem);
        }

        public void Send(Key key)
        {
            if (Keyboard.PrimaryDevice != null)
            {
                if (Keyboard.PrimaryDevice.ActiveSource != null)
                {
                    var e1 = new KeyEventArgs(Keyboard.PrimaryDevice, Keyboard.PrimaryDevice.ActiveSource, 0, Key.Down) { RoutedEvent = Keyboard.KeyDownEvent };
                    InputManager.Current.ProcessInput(e1);
                }
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(ViewModel.SelectedFeedback.Progress);
        }
    }
}
