# Feedback Register
This is a Windows desktop application written with C# and .NET Framework to register all feedback received by the school, Curtin Singapore. Its main features include:
* Feedback management
* Sending acknowledgement email to feedback contributor
* Report generation

It uses Microsoft Access database file (.mdb) for database storage.
